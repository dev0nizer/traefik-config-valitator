import yaml
import json
from jsonschema import validate, ValidationError
import argparse
import traceback
import os
from sys import exit

FILE_PROVIDER_SCHEMA_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)),"schema","traefik-v2-file-provider.json")
CONFIG_SCHEMA_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)),"schema", "traefik-v2.json")

class bcolors:
    ok = '\033[92m'
    warning = '\033[93m'
    fail = '\033[91m'
    ends = '\033[0m'

def parse_args():
    parser = argparse.ArgumentParser(prog="yaml-validator")
    parser.add_argument('--config', help="Path to traefik.yml")
    parser.add_argument('--folder', help="Path to file provider folder")
    return parser.parse_args()

def validator(config_text, schema_text):
    try:
        validate(config_text, schema_text)
    except ValidationError:
        print (f"{bcolors.fail}Validation failed!{bcolors.ends}")
        print (f"{bcolors.fail}{traceback.format_exc()}{bcolors.ends}")
        return False
    else:
        print (f"{bcolors.ok}Validation passed!{bcolors.ends}")
        return True


def main():
    args = parse_args()
    config_file = args.config
    config_folder = args.folder

    with open(CONFIG_SCHEMA_PATH, 'r') as f:
        config_schema_text = json.load(f)
    
    with open(FILE_PROVIDER_SCHEMA_PATH, 'r') as f:
        file_provider_schema_text = json.load(f)

    fail_count = 0
    if (config_file != None):
        if (os.path.exists(config_file)):
            with open(config_file, 'r') as f:
                config_text = yaml.safe_load(f)
                print("Processing " + os.path.basename(config_file))
                if(not validator(config_text, config_schema_text)):
                    fail_count+=1
        else:
            print(f"{bcolors.fail}Config file not found{bcolors.ends}")
    else:
        print("Skipping config file validation as --config was not passed")
        

    if (config_folder != None):
        for filename in os.listdir(config_folder):
            file_full_path = os.path.join(config_folder, filename)
            print("Processing " + os.path.basename(file_full_path))
            with open(file_full_path, 'r') as f:
                config_text = yaml.safe_load(f)
                if (not validator(config_text, file_provider_schema_text)):
                    fail_count+=1
    else:
        print("Skipping folder validation as --folder was not passed")
    
    print ("==========================================================")
    if (fail_count > 0):
        print(f"{bcolors.fail}Errors were found{bcolors.ends}")
        exit(1)
    else:
        print(f"{bcolors.ok}No errors found{bcolors.ends}")
        exit(0)


if (__name__ == "__main__"):
    main()