OUT_FILE := traefik-config-valitator
all:
	pip install pyinstaller
	pip install -r requirements.txt
	python3 -m PyInstaller -F --add-data "schema:schema" --name ${OUT_FILE} traefik-config-valitator.py
	rm -r build